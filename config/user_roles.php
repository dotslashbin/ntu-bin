<?php

return [

	'authentication' 			=> [
		'login' 				=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher', 'Student'], 
		'password_reset'		=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher', 'Student'], 
	], 
	'statistics' 				=> [
		'schools' 				=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher'], 
		'students' 				=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher']
	], 
	'survey' 					=> [
		'participate'			=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher', 'Student'], 
		'create'				=> ['super_user', 'PI', 'CO-PI', 'RA'],
		'update'				=> ['super_user', 'PI', 'CO-PI', 'RA'], 
		'delete'				=> ['super_user', 'PI', 'CO-PI', 'RA'], 
		'view'					=> ['super_user', 'PI', 'CO-PI', 'RA'], 
	], 
	'page_sections' 			=> [
		'left_menubar'			=> ['super_user', 'PI', 'CO-PI', 'RA']
	], 
	'forum' 					=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher', 'Student'],
	'notification' 				=> ['super_user', 'PI', 'CO-PI', 'RA', 'Student'],
	'user_account_management' 	=> [
		'PI' 			=> [
			'create' 			=> ['super_user'], 
			'update' 			=> ['super_user'], 
			'delete' 			=> ['super_user'], 
			'view' 				=> ['super_user'], 
		], 
		'CO-PI' 		=> [
			'create' 			=> ['super_user', 'PI'], 
			'update' 			=> ['super_user', 'PI'], 
			'delete' 			=> ['super_user', 'PI'], 
			'view' 				=> ['super_user', 'PI'], 
		], 
		'RA'			=> [
			'create' 			=> ['super_user', 'PI', 'CO-PI'],
			'update' 			=> ['super_user', 'PI', 'CO-PI'],
			'delete' 			=> ['super_user', 'PI', 'CO-PI'],
			'view' 				=> ['super_user', 'PI', 'CO-PI'],
		], 
		'Teacher'		=> [
			'create' 			=> ['super_user', 'PI', 'CO-PI', 'RA'],
			'update' 			=> ['super_user', 'PI', 'CO-PI', 'RA'],
			'delete' 			=> ['super_user', 'PI', 'CO-PI', 'RA'],
			'view' 				=> ['super_user', 'PI', 'CO-PI', 'RA'],
		], 
		'Student'		=> [
			'create' 			=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher'],
			'update' 			=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher'],
			'delete' 			=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher'],
			'view' 				=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher'],
		], 
	], 
	'push_notification' 		=> ['super_user', 'PI', 'CO-PI', 'RA'],
	'faq' 						=> [
		'view' 					=> ['super_user', 'PI', 'CO-PI', 'RA', 'Teacher', 'Student'],
		'create' 				=> ['super_user', 'PI', 'CO-PI', 'RA'],
		'update' 				=> ['super_user', 'PI', 'CO-PI', 'RA'],
		'delete' 				=> ['super_user', 'PI', 'CO-PI', 'RA'],

	]

];