<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

use Log; 

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Temp
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'email'         => 'required',
            'first_name'    => 'required|string|max:50', 
            'last_name'     => 'required|string|max:50', 
            'password'      => 'sometimes|required', 
            'role'          => 'in:principal_investigator,co_principal_investigator,research_assistant,teacher,student'
        ];
    }

    /**
     * Returns responses with validation errors when applicable
     * @param  Validator $validator [description]
     * @return [type]               [description]
     */
    public function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
    
}
