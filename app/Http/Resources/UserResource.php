<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'id' => $this->id, 
        //     'email' => $this->email, 
        //     'first_name' => $this->first_name, 
        //     'last_name' => $this->last_name, 
        //     'sound' => $this->sound, 
        //     'active' => $this->active, 
        //     'role' => $this->role, 
        //     'status' => $this->status, 
        //     'school_id' => $this->school_id
        // ];
    }

    public function with($request) {
        return [
            'meta' => [
                'foo' => 'bar'
            ]
        ]; 
    }
}
