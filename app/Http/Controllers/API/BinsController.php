<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

////////////////
// Resources  //
////////////////
use App\Http\Resources\BinResource as BinResource;

///////////////
// Requests  //
///////////////
use Illuminate\Http\Request;
use App\Http\Requests\BinStoreRequest; 

//////////////
// Helpers  //
//////////////
use App\Helpers\BinsHelper;

/////////////
// Models  //
/////////////
use App\Models\Bin;

///////////////
// Resources //
///////////////
use App\Http\Resources\MessageResource; 

use Auth; 

use Log; 

class BinsController extends Controller
{

    /**
     * Bins - destroy
     *
     * Endpoint for deleting a single bin record
     *
     * @bodyParam    id    integer    required    id of the bin
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {

        $hasError = false;

        if($id && is_numeric($id) && $id != 0) {
            $result = BinsHelper::deleteBin($id); 

            if($result) {
                /**
                 * Returns an 'id' key to adhere to the react-admin accepted format
                 */
                return new MessageResource([ 'id' => $id, 'message' => 'Successfully deleted bin', 'status' => 'success']); 
            }
        }

        if($hasError === true) {
            return new MessageResource([ 'message' => 'Failed to delete bin record', 'status' => 'fail']); 
        }
    }

     /**
     * Bins - Delete Many
     *
     * Deletes multiple bins given an array of integers as bin_ids.
     *
     * @bodyParam     array    integer    required    Bin Ids
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroyMany(Request $request)
    {
        $binIds = $request->get('ids'); 

        BinsHelper::deleteBins($binIds); 

        /**
         * Returns an empty array adhere to the react-admin format
         */
        return new MessageResource([]);
    }

    /**
     * Bins - getToken 
     *
     * Fetches an authentication token for a bin. 
     *
     * @bodyParam     bin_id    integer   required    Id of the bin 
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getToken(Request $request) {
        return BinsHelper::generateBinToken($request->get('id'));
    }

    /**
     * Bins - fetch 
     *
     * Returns a collection of bins. When a "school_id" is supplied, only 
     * binsd associated with the school are returned
     *
     * @bodyParam    items_per_page    integer      Items per page for
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        ////////////////////////////////////////
        // Filter: show "available" bins only //
        ////////////////////////////////////////
        if($request->query('available_only') && $request->query('available_only') == 1) {
            return BinsHelper::generateFreeBins(); 

        } elseif($request->query('filter') != null || $request->query('filter') != '') {

            $filterString = $request->query('filter'); 

            $filterObject = json_decode($filterString); 

            //////////////////////////////////////////////////////////
            // Filter: show all bins as this is for lookup purposes //
            //////////////////////////////////////////////////////////
            if(isset($filterObject->lookup) && $filterObject->lookup == 1) {
                return BinsHelper::generateBins(null); 
            }

            ////////////////////////////////////////////
            // Filter: selected IDs ( for GET_MANY )  //
            ////////////////////////////////////////////
            if(isset($filterObject->id) && $filterObject->id) {

                // Create filter for selected ids
                $query = [ 'selected_ids' => $filterObject->id ]; 
                return BinsHelper::generateBins($query); 
            }

        } else {

            $query = [
                'school_id'         => $request->query('school_id'), 
                'items_per_page'    => $request->query('items_per_page')
            ]; 

            return BinsHelper::generateBins($query);        
        }

    	
    }

    /**
     * Bins - show
     *
     * Returns a bin object resource based on the given "bin_id" 
     *
     * @bodyParam       bin_id      integer     required    Record id of the bin
     * 
     * @param  Request $request [description]
     * @param  [type]  $binId   [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $binId) {
        return BinsHelper::getBin($binId); 
    }

    /**
     * Bins - store
     *
     * Endpoint for creating a bin record
     *
     * @bodyParam    school_id    integer    required    bin's ID
     * 
     * @param  BinStoreRequest $request [description]
     * @return [type]                   [description]
     */
    public function store(BinStoreRequest $request) {
        $result = null;

        $result = BinsHelper::createBin($request->all()); 

        if($result) {
            return $result;
        }
    }

    /**
     * Bins - update
     *
     * Endpoint to update a bin record
     *
     * @bodyParam    school_id    rquired    integer    bin's record ID
     * 
     * @param  BinStoreRequest $request [description]
     * @return [type]                   [description]
     */
    public function update(BinStoreRequest $request, $id)
    {
        $result = null; 

        $result = BinsHelper::updateBin($id, $request->all());

        if($result) {
            return $result; 
        } 
    }
}
