<?php

namespace App\Http\Controllers;

////////////////
// Resources  //
////////////////
use App\Http\Resources\UserResource as UserResource;
use App\Http\Resources\MessageResource as MessageResource;

///////////////
// Requests  //
///////////////
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserGetRequest;
//////////////
// Helpers  //
//////////////
use App\Helpers\Common;
use App\Helpers\UsersHelper;

/////////////
// Models  //
/////////////
use App\Models\User;

use Log;
use Auth; 

class UsersController extends Controller
{
    
    /**
     * User - Delete single
     *
     * Deletes a single user account
     *
     * @bodyParam  id  required  integer  User's Id
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
	public function destroy(Request $request, $id)
	{

        $hasError = false;

        if($id && is_numeric($id) && $id != 0) {

            /**
             * Special case for teachers. They should only be allowed to delete students (users) 
             * from the same school.
             */
            if(Auth::user()->hasRole('teacher') && !UsersHelper::isInTheSameSchool($id, Auth::user()->school_id)) {

                return Common::returnUnauthorizedError(); 
            }

            if( UsersHelper::deleteUser($id) )
            {
                /**
                 * Returns an 'id' key to adhere to the react-admin accepted format
                 */
                return new MessageResource([ 'id' => $id, 'message' => 'Successfully deleted user', 'status' => 'success']); 
            }
        } 

        return new MessageResource([ 'message' => 'Failed to delete user record', 'status' => 'fail']); 

	}

    /**
     * User - Delete Many
     *
     * Deletes multiple user accounts given an array of integers as user_ids.
     *
     * @bodyParam     array    required    integer    User ids
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroyMany(Request $request)
    {
        $userIds = $request->get('ids'); 

        UsersHelper::deleteUsers($userIds); 

        /**
         * Returns an empty array adhere to the react-admin format
         */
        return new MessageResource([]);
    }

    /**
     * User - fetch collection
     * 
     * Returns a collection of resource user objects
     *
     * @bodyParam   items_per_page    integer    Indicates how many items per page
     * @bodyParam   page              integer    Current page
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request)
    {
        $query = [
            'roles'             => $request->get('roles'), 
            'items_per_page'    => $request->query('items_per_page')
        ];

        return UsersHelper::generateUsers($query);  
    }

    /**
     * User - show
     * 
     * Returns the profile of the user derrived from the provided user id. If no user_id is provied, the profile of the authenticated user 
     * is returned. NOTE: This may be deprecated soon.
     *
     * @bodyParam   user_id integer     User id 
     * 
     * @param  Request $request [description]
     * @param  [type]  $userId  [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $userId) {
        $userId = ($userId)? $userId:Auth::user()->id; 
        return UsersHelper::getUser($userId); 
    }

    /**
     * User - save
     * 
     * API endpoint to save a user profile.
     *
     * @bodyParam   first_name      string      required    First Name
     * @bodyParam   last_name       string      required    Last Name
     * @bodyParam   email           string      required    User's Email
     * @bodyParam   password        string      required    Password
     * @bodyParam   role            string      required    User's role.
     * @bodyParam   school_id       integer                 Id of the school to be associated with.
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(UserStoreRequest $request)
    {
        $role = ($request->get('role'))? $request->get('role'):config('app.app_defaults.default_role');

        if(UsersHelper::canExecuteOnAccount($role, 'create')) {
           $result = UsersHelper::createUser($request->all()); 

           if($result) {
                return $result; 
            } 
        } else {
            return Common::returnUnauthorizedError(); 
        }
    }

    /**
     * Users - update
     *
     * Endpoint for updating a user. 
     *
     * @bodyParam   first_name      string      First Name
     * @bodyParam   last_name       string      Last Name
     * @bodyParam   email           string      User's Email
     * @bodyParam   password        string      Password
     * @bodyParam   role            string      User's role.
     * @bodyParam   school_id       integer     Id of the school to be associated with.
     * 
     * @param  UserStoreRequest $request [description]
     * @param  [type]           $id      [description]
     * @return [type]                    [description]
     */
    public function update(UserStoreRequest $request, $id)
    {
        $result = null; 

        $role = ($request->get('role'))? $request->get('role'):config('app.app_defaults.default_role');

        if(UsersHelper::canExecuteOnAccount($role, 'update')) {
            $result = UsersHelper::updateUser($id, $request->all()); 

            if($result) {
                return $result; 
            }    
        } else {
            return Common::returnUnauthorizedError(); 
        }
    }
}
