<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

////////////////
// Resources  //
////////////////
use App\Http\Resources\TrashCollection as TrashCollection; 
use App\Http\Resources\TrashResource as TrashResource; 

///////////////
// Requests  //
///////////////
use Illuminate\Http\Request;
use App\Http\Requests\TrashStoreRequest; 

/////////////
// Helpers //
/////////////
use App\Helpers\TrashesHelper;

/////////////
// Models  //
/////////////
use App\Models\Trash; 

///////////////
// Resources //
///////////////
use App\Http\Resources\MessageResource; 

use Auth; 

use Log; 

class TrashesController extends Controller
{
	/**
     * Trash - destroy
     *
     * Endpoint for deleting a single trash record
     *
     * @bodyParam    id    integer    required    id of the trash
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {

        $hasError = false;

        if($id && is_numeric($id) && $id != 0) {
            $result = TrashesHelper::deleteTrash($id); 

            if($result) {
                /**
                 * Returns an 'id' key to adhere to the react-admin accepted format
                 */
                return new MessageResource([ 'id' => $id, 'message' => 'Successfully deleted trash', 'status' => 'success']); 
            }

            $hasError = true;

        }

        if($hasError === true) {
            return new MessageResource([ 'message' => 'Failed to delete trash record', 'status' => 'fail']); 
        }
    }

    /**
     * Trashes - Delete Many
     *
     * Deletes multiple trash records given an array of integers as trash_ids.
     *
     * @bodyParam     array    integer     required    trash Ids
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroyMany(Request $request)
    {
        $binIds = $request->get('ids'); 

        TrashesHelper::deleteTrashes($binIds); 

        /**
         * Returns an empty array adhere to the react-admin format
         */
        return new MessageResource([]);
    }
	
	/**
	 * Trashes - fetch 
	 *
	 * Endpoint to fetch a collection of trashes. Results can be filtered
	 * according to the supplied parameters
	 *
     * @bodyParam    bin_id         integer     trash's ID
     * @bodyParam    items_per_page integer     Items per page
	 * 
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function index(Request $request)
	{
		$query = [
    		'bin_id' 		=> $request->query('bin_id'), 
    		'items_per_page' 	=> $request->query('items_per_page')
    	]; 

    	return TrashesHelper::generateTrashes($query); 
	}

	/**
	 * Trash - show
	 *
	 * Returns a trash resource object
	 *
	 * @bodyParam    integer     trash_id    required    ID of the record
	 * 
	 * @param  Request $request [description]
	 * @param  [type]  $trashId [description]
	 * @return [type]           [description]
	 */
	public function show(Request $request, $trashId) 
	{
		return TrashesHelper::getTrash($trashId); 
	}

	 /**
     * Trash - store
     *
     * Endpoint for creating a trash record
     *
     * @bodyParam    bin_id         integer     required    trash's ID
     * @bodyParam    selected_type  string      required    Type of trash
     * @bodyParam    thrown_type    string      required    Thrown type 
     * @bodyParam    weight         float       required    weight of the trash
     * @bodyParam    picture        string      required    base 64 image of the trash
     *
     * @param  BinStoreRequest $request [description]
     * @return [type]                   [description]
     */
    public function store(TrashStoreRequest $request) {
        $result = null;

        $result = TrashesHelper::createTrash($request->all()); 

        if($result) {
            return $result;
        }
    }

     /**
     * Trashes - update
     *
     * Endpoint to update a trash record. 
     *
     * @bodyParam    bin_id         integer     trash's ID
     * @bodyParam    selected_type  string      Type of trash
     * @bodyParam    thrown_type    string      Thrown type 
     * @bodyParam    weight         float       weight of the trash
     * @bodyParam    picture        string      base 64 image of the trash
     * 
     * @param  BinStoreRequest $request [description]
     * @return [type]                   [description]
     */
    public function update(TrashStoreRequest $request, $id)
    {
        $result = null; 

        $result = TrashesHelper::updateTrash($id, $request->all());

        if($result) {
            return $result; 
        } 
    }
}
