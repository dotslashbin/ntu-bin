<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//////////////
// Helpers  //
//////////////
use App\Helpers\StatisticsHelper; 

use Log; 

class StatisticsController extends Controller
{
    public function index(Request $request)
    {
    	 $query = [
            'items_per_page'    => $request->query('items_per_page')
        ];

        return StatisticsHelper::generateSatistics($query); 
    }
}
