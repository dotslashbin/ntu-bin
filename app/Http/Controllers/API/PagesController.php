<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/////////////
// Helpers //
/////////////
use App\Helpers\PagesHelper; 

class PagesController extends Controller
{
	/**
	 * Pages - Index 
	 *
	 * Returns all the pages. 
	 * 
	 * @return [type] [description]
	 */
    public function index()
    {
    	return PagesHelper::generatePages(); 
    }

    /**
     * Pages - update 
     *
     * Updates a page
     *
     * @bodyParam   id 			integer 	required 	Record ID
     * @bodyParam 	content 	string 		required 	Content of the page
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function update(Request $request, $id)
    {
    	$result = null; 

        $result = PagesHelper::updatePage($id, $request->all());

        if($result) {
            return $result; 
        } 
    }

    /**
     * Pages - show
     *
     * Returns a page
     *
     * @bodyParam   id 			integer 	required 	Record ID
     *
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id)
    {
    	return PagesHelper::getPage($id); 
    }
}
