<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//////////////
// Helpers  //
//////////////
use App\Helpers\RolesHelper;

use Log; 

class RolesController extends Controller
{
	/**
	 * Roles - Index
	 *
	 * Endpoint to fetch available user roles ( except the super-admin )
	 * 
	 * @return [type] [description]
	 */
    public function index(Request $request)
    {
    	$limitPrivilege = ($request->query('limit_privilege') && $request->query('limit_privilege') == 1)? true:null; 
			
		return RolesHelper::generateRoles($limitPrivilege);     	
    }
}
