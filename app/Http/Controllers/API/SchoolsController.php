<?php

namespace App\Http\Controllers;

////////////////
// Resources  //
////////////////
use App\Http\Resources\SchoolResource as SchoolResource;
use App\Http\Resources\MessageResource as MessageResource;

///////////////
// Requests  //
///////////////
use Illuminate\Http\Request;
use App\Http\Requests\SchoolStoreRequest;


//////////////
// Helpers  //
//////////////
use App\Helpers\SchoolsHelper; 

/////////////
// Models  //
/////////////
use App\Models\School;

use Log; 

class SchoolsController extends Controller
{
    /**
     * School - Delete
     *
     * Deletes a single school account
     *
     * @bodyParam  id  integer  required    schools' Id. 
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function destroy(Request $request, $id)
    {

        $hasError = false;

        if($id && is_numeric($id) && $id != 0) {
            $result = SchoolsHelper::deleteSchool($id); 

            if($result) {
                /**
                 * Returns an 'id' key to adhere to the react-admin accepted format
                 */
                return new MessageResource([ 'id' => $id, 'message' => 'Successfully deleted school', 'status' => 'success']); 
            }

            $hasError = true;
        }

        if($hasError === true) {
            return new MessageResource([ 'message' => 'Failed to delete school record', 'status' => 'fail']); 
        }
    }

    /**
     * User - Delete Many
     *
     * Deletes multiple school accounts given an array of integers as school_ids.
     *
     * @bodyParam     array    integer      required    User ids
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function destroyMany(Request $request)
    {
        $schoolIds = $request->get('ids'); 

        SchoolsHelper::deleteSchools($schoolIds); 

        /**
         * Returns an empty array adhere to the react-admin format
         */
        $returnObject = new \stdClass; 
        $returnObject->message = "Successfully deleted schools"; 
        return new MessageResource([$returnObject]);
    }

    /**
     * School - Fetch
     *
     * Returns a collection of school resource objects
     *
     * @bodyParam    items_per_page    integer       
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function index(Request $request) {

        $query = [
            'items_per_page' => $request->get('items_per_page')
        ]; 

        return SchoolsHelper::generateShools($query); 
    }

    /** 
     * School - Show
     *
     * Fetches a single school record given the ID
     *
     * @bodyParam   school_id   integer     required    Record id of the school
     * 
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function show(Request $request, $id) {
        return SchoolsHelper::getSchool($id); 
    }

    /**
     * School - save
     * 
     * API endpoint to create a school profile.
     *
     * @bodyParam    name       string      required    Name of the school
     * @bodyParam    address    string      required    Address of the school
     * 
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(SchoolStoreRequest $request)
    {
        $result = null; 

        $result = SchoolsHelper::createSchool($request->all());

        if($result) {
            return $result; 
        } 
    }

    /**
     * School - update
     *
     * Endpoint for updating a school.
     *
     * @bodyParam    name       string      required    Name of the school
     * @bodyParam    address    string      required    Address of the school
     * 
     * @param  SchoolStoreRequest $request [description]
     * @param  [type]             $id      [description]
     * @return [type]                      [description]
     */
    public function update(SchoolStoreRequest $request, $id)
    {
        $result = null; 

        $result = SchoolsHelper::updateSchool($id, $request->all());

        if($result) {
            return $result; 
        } 
    }
}
