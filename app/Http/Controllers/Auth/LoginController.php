<?php
use App\Models\User; 
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use Log;

class LoginController extends Controller
{
    public $successStatus = 200; 

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * User - login
     * 
     * Endpoint to authenticate a user. It uses email and password combination
     *
     * @bodyParam   email   string  User's email
     * @bodyParam   password   string  User's password
     */
    public function store() {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')]) && Auth::user()->active == true){ 
            $user = Auth::user(); 

            $returnObject               = new \stdClass; 
            $returnObject->data         = new \stdClass; 
            $returnObject->data->token  = $user->createToken('alphanova')->accessToken; 
            $returnObject->data->role   = $user->role_name; 
            $returnObject->status       = $this->successStatus; 
            return response()->json($returnObject, $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
        /* NOTE: This was tymon jwt implementation
        $credentials = request(['email', 'password']);

        Log::info("CREDENTIALS: ".json_encode($credentials)); 

        if (!$token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return response()->json([
            'token' => $token,
            'expires' => auth('api')->factory()->getTTL() * 60,
        ]);
        */
    }
}
