<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Bin;

class School extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name'];

    protected $appends = ['bin_ids', 'associated_bins'];

    /**
     * Returns  a collection of related bins. 
     * 
     * @return [type] [description]
     */
    public function bins()
    {
    	return $this->hasMany('App\Models\Bin', 'school_id', 'id');
    }

    public function getAssociatedBinsAttribute() 
    {
        return Bin::where('school_id', $this->id)->get(); 
    }

    public function getBinIdsAttribute()
    {
        return Bin::where('school_id', $this->id)->get()->pluck('id')->toArray(); 
    }
}