<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    protected $fillable = ['selected_type', 'thrown_type', 'weight', 'picture'];

    /**
     * Returns the bin object related to the trash
     * @return [type] [description]
     */
    public function bin()
    {
    	return $this->belongsTo('App\Models\Bin', 'bin_id', 'id');
    }

    /**
     * Returns the user object related to the trash. 
     * @return [type] [description]
     */
    public function user()
    {
		return $this->belongsTo('App\Models\User', 'user_id', 'id'); 
    }
}
