<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/////////////
// Models  //
/////////////
use \Spatie\Permission\Models\Role;

///////////
// Role  //
///////////
Use Spatie\Permission\Traits\HasRoles; 

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable, SoftDeletes;

    protected $guard_name = 'web'; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['role_id', 'role_name', 'finger_print_file'];

    public function getFingerPrintFileAttribute()
    {
        return config('app.app_defaults.finger_print_storage').$this->finger_print;
    }

    /**
     * Returns the name of the role for the particular users. 
     *
     * For this scenario, we only return one, the first one. This might be
     * revised if we ever decide to support multiple roles for a user. 
     * 
     * @return [type] [description]
     */
    public function getRoleAttribute()
    {
        if(count($this->getRoleNames()) > 0 ) {
            return $this->getRoleNames()[0];     
        }
        
        return null;  
    }

    /**
     * Returns the id the first role the user is assigned to. 
     * NOTE: this will have to be changed if we decide to support multiple roles. 
     * 
     * @return [type] [description]
     */
    public function getRoleIdAttribute() {
        if(isset($this->getRoleNames()[0])) {
            return Role::where('name', $this->getRoleNames()[0])->pluck('id')->toArray()[0];    
        }

        return null; 
    }

    /**
     * Returns the name the first role the user is assigned to. 
     * NOTE: this will have to be changed if we decide to support multiple roles. 
     * 
     * @return [type] [description]
     */
    public function getRoleNameAttribute()
    {
        if(isset($this->getRoleNames()[0])) {
            return $this->getRoleNames()[0]; 
        }

        return null; 
    }

    /**
     * Returns the related school of the user
     * @return [type] [description]
     */
    public function school() {
        return $this->hasOne('App\Models\School', 'id', 'school_id'); 
    }
}
