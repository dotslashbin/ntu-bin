<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Bin extends Authenticatable
{
    use HasApiTokens; 
	///////////////
	// Mutators  //
	///////////////
	protected $appends = ['name', 'code']; 

    public function school() {
    	return $this->belongsTo('App\Models\School', 'school_id', 'id'); 
    }

    /**
     * Generates the code/name from the given ID. 
     *
     * NOTE: This is still under construction, there should be a logic that generates 
     * the codenames. 
     *
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function getCodeName($id) {
        return "B000".$id;
    }

    /**
     * Generates a code from the bin's ID
     * @return [type] [description]
     */
    public function getCodeAttribute() {
        return $this->getCodeName($this->id); 
    }

    /**
     * Generates a name from the bin ID
     * @return [type] [description]
     */
    public function getNameAttribute()
    {
        return $this->getCodeName($this->id); 
    }
}
