<?php
namespace App\Helpers;

//////////////
// Helpers  //
//////////////
use App\Helpers\Common; 

/////////////
// Models  //
/////////////
use App\Models\Bin; 

////////////////
// Resources  //
////////////////
use App\Http\Resources\BinCollection; 
use App\Http\Resources\BinResource; 

use Auth;

use Log; 

class BinsHelper {

	/**
	 * Method to create a new bin record 
	 * 
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function createBin($params) 
	{
		$bin = new Bin(); 

		self::saveBinProperties($bin, $params);

		if($bin->save()) {
			return new BinResource(Bin::with('school')->find($bin->id));
		}

		return null; 
	}

	/** 
	 * Deletes a single bin object
	 */
	public static function deleteBin($id)
	{
		return Bin::destroy((integer) $id); 	
	}

	/**
	 * Deletes multiple bin records 
	 * @param  Array  $ids [description]
	 * @return [type]      [description]
	 */
	public static function deleteBins(Array $ids)
    {
        $hadNoProblems = true; 
        Bin::find($ids)->each(function($bin) {
            if(!$bin->delete()) {
                Log::warning("ERROR: deleting bin -> ".$bin->id);
                $hadNoProblems = false; 
            } 
        }); 

        return $hadNoProblems; 
    }

	/**
	 * Returns a collection of bins derived from the given filters
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public static function generateBins($filters)
	{

		if($filters === null) {
			return new BinCollection(Bin::paginate(Bin::count())); 
		}

		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page'); 

		$results = null; 

		$bins = Bin::with('school'); 
		if(isset($filters['school_id']) && $filters['school_id'] != null) {
			$bins->where('school_id', $filters['school_id']);
		}

		if(isset($filters['selected_ids']) && (count($filters['selected_ids']) > 0)) {
			$bins->whereIn('id', $filters['selected_ids']); 
		}

		$results = $bins->paginate($itemsPerPage); 
		return new BinCollection($results); 
	}

	/**
	 * Returns a generated JWT token for a bin to use to make API calls. 
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public static function generateBinToken($id)
	{	
		if(is_numeric($id))
		{
			$bin = Bin::find((int) $id); 

			if($bin) {

				$returnObject               	= new \stdClass; 
	            $returnObject->data         	= new \stdClass; 
	            $returnObject->data->token  	= $bin->createToken('alphanova')->accessToken; 
	            $returnObject->data->role   	= 'bin'; 
	            $returnObject->data->school_id 	= $bin->school_id; 
	            return response()->json($returnObject, 200); 
			}
		}

		$returnObject               = new \stdClass; 
    	$returnObject->data         = new \stdClass; 

		return response()->json($returnObject, 200); 
	}

	/**
	 * Generates a collection of bins that are tied to any school. 
	 * 
	 * @return [type] [description]
	 */
	public static function generateFreeBins() {
		return new BinCollection(Bin::whereNull('school_id')->get()); 
	}

	/**
	 * Return a bin object resource with the related school object attached
	 * 
	 * @param  [type] $binId [description]
	 * @return [type]        [description]
	 */
	public static function getBin($binId)
	{
		return new BinResource(Bin::with('school')->find($binId)); 
	}

	/**
	 * Initiates the properites for the bin object ot be saved. 
	 * 
	 * @param  [type] $bin    [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	private static function saveBinProperties($bin, $params)
	{
		if(isset($params['school_id']) && $params['school_id'] > 0) {
			$bin->school_id 	= (integer) $params['school_id'];
		}
	}

	/**
	 * Updates a bin record given the provided parameters and ID. 
	 * 
	 * @param  [type] $id     [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function updateBin($id, $params)
	{

		$bin = Bin::find((int) $id); 

		self::saveBinProperties($bin, $params); 
		
		if($bin->save()) {
			return new BinResource($bin); 
		}

		return null; 
	}
}