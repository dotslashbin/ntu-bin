<?php
namespace App\Helpers;

//////////////
// Helpers  //
//////////////
use App\Helpers\Common; 

////////////
// Models //
////////////
use App\Models\Bin;
use App\Models\School;

////////////////
// Resources  //
////////////////
use App\Http\Resources\SchoolCollection; 
use App\Http\Resources\SchoolResource; 

use Auth, DB; 

use Log; 

class SchoolsHelper {
	
	private static function attachBinsToSchool($bins, $school) {
		foreach($bins as $binId) {

			$bin = Bin::find($binId); 

			$bin->school_id = $school->id; 
			if(!$bin->save()) {
				Log::warning("ERROR: there was a problem attaching bin: ".$binId." to school: ".$school->id); 
			}
		}
	}

	/**
	 * Calls to create a new school record
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function createSchool($params)
	{
		$school = new School(); 
		self::saveSchoolProperties($school, $params); 		

		if($school->save()) {

			////////////////////////////
			// If bins were selected  //
			////////////////////////////
			if(isset($params['bins']) && count($params['bins']) > 0) {
				self::attachBinsToSchool($params['bins'], $school);
			}

			return new SchoolResource($school); 
		}

		return null; 
	}

	/**
     * Deletes a single school account
     * @param  Integer $id [description]
     * @return [type]      [description]
     */
    public static function deleteSchool($id) 
    {
        return School::destroy((integer)$id); 
    }

	/**
     * Deletes school records from an array of ids given
     * @param  [type] $ids [description]
     * @return [type]      [description]
     */
    public static function deleteSchools(Array $ids)
    {
        $hadNoProblems = true; 
        School::find($ids)->each(function($school) {
            if(!$school->delete()) {
                Log::warning("ERROR: deleting school -> ".$school->id);
                $hadProblems = false; 
            } 
        }); 

        return $hadNoProblems; 
    }

	/**
	 * Returns a collection of schools based on the given filters
	 * 
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public static function generateShools($filters)
	{
		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page'); 

		$schools = School::paginate($itemsPerPage); 

		return new SchoolCollection($schools); 
	}

	/**
	 * Returns a school resource object
	 * 
	 * @param  [type] $schoolId [description]
	 * @return [type]           [description]
	 */
	public static function getSchool($schoolId)
	{
		return new SchoolResource(School::with('bins')->find($schoolId)); 
	}

	private static function removeBinsFromSchool($schoolId)
	{
		Bin::where('school_id', (integer)$schoolId)->update([ 'school_id' => null ]); 
	}

	/**
	 * Assignes properties to the school object for saving
	 * 
	 * @param  [type] $school   [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	private static function saveSchoolProperties($school, $params) {
		$school->name  	 = ($params['name'])? trim($params['name']):''; 
		$school->address = ($params['address'])? trim($params['address']):''; 
	}

	/**
	 * Calls to update a school record
	 * @param  [type] $id     [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function updateSchool($id, $params)
	{
		$school = School::find((int) $id); 
		self::saveSchoolProperties($school, $params); 
		
		if($school->save()) {
			self::removeBinsFromSchool($school->id); 
			self::attachBinsToSchool($params['bin_ids'], $school);

			return new SchoolResource($school); 
		}

		return null; 
	}
} 