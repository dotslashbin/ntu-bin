<?php
////////////////
// Namespaces //
////////////////
namespace App\Helpers;

/////////////
// Models  //
/////////////
use App\Models\User;

use Auth; 

class Common {

	/**
	 * Removes a defined prefix from every element of a given array
	 * @param  [type] $prefix [description]
	 * @param  [type] $input  [description]
	 * @return [type]         [description]
	 */
	public static function removePrefixFromElements($prefix, $input) {

		$container = []; 

		foreach($input as $element) {
			list($prefix, $value) = explode(' ', $element); 

			array_push($container, $value); 
		}

		return $container; 
	}

	/**
	 * Returns a JSON string that follows the "reference" format 
	 * required by react-admin. 
	 * 
	 * @param  [type] $collection [description]
	 * @param  [type] $total      [description]
	 * @return [type]             [description]
	 */
	public static function renderReferenceFormat($collection, $total)
	{
		$returnObject 			= new \stdClass; 
		$returnObject->data 	= $collection; 
		$returnObject->total 	= $total; 

		return response()->json($returnObject); 
	}

	/**
	 * Returns a 401 status with a message. 
	 * 
	 * @return [type] [description]
	 */
	public static function returnUnauthorizedError() 
	{
		$returnObject = new \stdClass; 
		$returnObject->message = 'You are not autorized to do this.';
		return response()->json($returnObject, 401); 
	}
}
