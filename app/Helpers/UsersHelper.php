<?php
namespace App\Helpers;

//////////////
// Helpers  //
//////////////
use App\Helpers\Common; 

////////////
// Models //
////////////
use App\Models\User;

////////////////
// Resources  //
////////////////
use App\Http\Resources\UserCollection; 
use App\Http\Resources\UserResource; 

//////////////
// Facades  //
//////////////
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use Auth, DB; 

use Log; 

class UsersHelper{
	/**
	 * Iterates through the user collection and maps the roles to each of them. 
	 * 
	 * @param  [type] $users [description]
	 * @return [type]        [description]
	 */
	private static function attachRoles($users) {
		foreach($users as $user) {
			if(isset($user->getRoleNames()[0])) {
				$role = $user->getRoleNames()[0]; 
				$user->role = $role; 
			} 

			$user->role = null; 
			
		}

		return $users; 
	}

	public static function canExecuteOnAccount($roleToHandle, $permissionToCheck)
	{
		$user = Auth::user(); 
		return $user->hasPermissionTo($permissionToCheck.' '.$roleToHandle); 
	}

	/**
	 * Calls to create a new user record
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function createUser($params)
	{
		$user = new User(); 
		self::saveUserProperties($user, $params); 	

		//////////////////
		// Fingerprint  //
		//////////////////
		if(isset($params['finger_print']) && $params['finger_print'] != '') {
			$user->finger_print = self::createFingerprintFile($params['finger_print'], $user); 
		}

		if($user->save()) {
			return new UserResource(User::find($user->id)); 
		} 

	}

	/**
	 * Returns the hashed filename of the uploaded file. 
	 * 
	 * @param  [type] $encodedString [description]
	 * @param  [type] $user          [description]
	 * @return [type]                [description]
	 */
	private static function createFingerprintFile($encodedString, $user) 
	{
		if(is_string($encodedString))
		{
			list ($mime, $base64String ) = explode( ',', $encodedString ); 

			$image = base64_decode($base64String); 

			$fileName = md5($user->id.$user->email); 
			
			Storage::disk('finger_prints')->put($fileName, $image); 

			return $fileName; 	
		} 

		return ''; 
	}

	/**
	 * Deletes a single user account
	 * @param  Integer $id [description]
	 * @return [type]      [description]
	 */
	public static function deleteUser($id) {
		return User::destroy((integer)$id); 
	}

	/**
	 * Deletes user records from an array of ids given
	 * @param  [type] $ids [description]
	 * @return [type]      [description]
	 */
	public static function deleteUsers(Array $ids)
	{
		$hadNoProblems = true; 
		User::find($ids)->each(function($user) {

			/**
			 * Special case for teachers. They can only delete the accounts 
			 * of students that belong to the same school. 
			 */
			if(Auth::user()->hasRole('teacher') && !self::isInTheSameSchool($user->id, Auth::user()->school_id)) {
				$hadProblems = false; 
			} else {
				if(!$user->delete()) {
	                Log::warning("ERROR: deleting user -> ".$user->id);
	                $hadProblems = false; 
	            } 	
			}
        }); 

        return $hadNoProblems; 
	}

	/**
	 * Delete's a file given the full file path. 
	 * 
	 * @param  [type] $fullFilePath [description]
	 * @return [type]               [description]
	 */
	private static function deleteOldFile($fullFilePath) {
		Storage::delete($fullFilePath); 
	}

	/**
	 * Returns a set of users based on the conditions and filters
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public static function generateUsers($filters)
	{
		/**
		 * TODO: 
		 *
		 * implement filters
		 */
		
		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page'); 

		$user = Auth::user(); 

		if($user->hasRole('super-admin')) {
			
			//////////////////
			// Super Admin  //
			//////////////////
			// return new UserCollection(User::paginate($itemsPerPage));
			$users = User::where('id', '>', 1)->paginate($itemsPerPage); 

		} else {
			
			/////////////////
			// Normal User //
			/////////////////
			$users = User::whereHas("roles", function($query) { 
				$query->whereIn("name", self::getViewableUserRoles()); 

				if(Auth::user()->hasRole('teacher')) {
					$query->where('school_id', Auth::user()->school_id);	
				}
				
			})->paginate($itemsPerPage); 
		}

		return new UserCollection(self::attachRoles($users)); 
	}

	/**
	 * Returns a user object from the supplied id
	 * 
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public static function getUser($userId)
	{
		return new UserResource(User::with('school')->find($userId));
	}
	

	/**
	 * Returns an array of of roles that are vieable by a given user
	 *
	 * NOTE: this can be improved by allowing different actinos (view, update, create)
	 * 
	 * @return [type] [description]
	 */
	private static function getViewableUserRoles() {

		$permissionIds = Auth::user()->getAllPermissions()->pluck('id')->toArray(); 

		$listOfPermissions = DB::table('permissions')->whereIn('id', $permissionIds)->where('name', 'like', 'view %')->pluck('name')->toArray(); 

		$cleanedResult = Common::removePrefixFromElements('view', $listOfPermissions); 

		return $cleanedResult; 
	}

	/**
	 * Check if the user is of the same school as the given school id. 
	 * 
	 * @param  [type]  $userId   [description]
	 * @param  [type]  $schoolId [description]
	 * @return boolean           [description]
	 */
	public static function isInTheSameSchool($userId, $schoolId)
	{
		return (User::where('id', $userId)->where('school_id', $schoolId)->count())? true:false; 
	}

	/**
	 * Assignes properties to the user object for saving
	 * @param  [type] $user   [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	private static function saveUserProperties($user, $params) {
		$user->first_name  	= ($params['first_name'])?trim($params['first_name']):''; 
		$user->last_name  	= ($params['last_name'])?trim($params['last_name']):''; 
		$user->email 		= ($params['email'])? trim($params['email']):''; 

		if(isset($params['password'])) {
			$user->password  	= bcrypt(trim($params['password'])); 	
		}
		
		////////////
		// Roles  //
		////////////
		if(isset($params['role'])) {

			//////////////////////////////////////
			// Check if user already has a role //
			//////////////////////////////////////
			if(count($user->getRoleNames()) > 0) {
				$user->removeRole($user->getRoleNames()[0]); 
			}
			$user->assignRole($params['role']);
			
		} else { 
			//////////////////////////////////////////////
			// Assigns the role of 'student' by default //
			//////////////////////////////////////////////
			$user->assignRole('student');
		}

		/////////////
		// School  //
		/////////////
		if(isset($params['school_id'])) {
			$user->school_id = $params['school_id']; 
		}

		///////////////////////////
		// Special Case: Teacher //
		///////////////////////////
		/**
		 * If a teacher registers a student, they will carry the same 
		 * school as the teacher
		 */
		if(Auth::user()->hasRole('teacher')) {
			$user->school_id = Auth::user()->school_id; 
		}
	}

	/**
	 * Calls to update a user record
	 * @param  [type] $id     [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function updateUser($id, $params)
	{

		$user 				= User::find((int) $id); 

		self::saveUserProperties($user, $params); 

		//////////////////
		// Fingerprint  //
		//////////////////
		if(isset($params['finger_print']) && $params['finger_print'] != '') {
			$user->finger_print = self::createFingerprintFile($params['finger_print'], $user); 
		}

		if($user->save()) {
			return new UserResource($user); 
		}
	}
} 
