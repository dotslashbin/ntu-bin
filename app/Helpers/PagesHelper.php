<?php
////////////////
// Namespaces //
////////////////
namespace App\Helpers;

/////////////
// Models  //
/////////////
use App\Models\Page;

///////////////
// Resources //
///////////////
use App\Http\Resources\PageCollection; 
use App\Http\Resources\PageResource; 

use Auth; 

class PagesHelper {

	public static function getPage($pageId)
	{
		return new PageResource(Page::find($pageId)); 
	}

	/**
	 * Returns the content of FAQ page
	 * 
	 * @return [type] [description]
	 */
	public static function generatePages() {

		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page'); 

		return new PageCollection(Page::paginate($itemsPerPage)); 
	}

	/**
	 * Updates a page 
	 * 
	 * @param  [type] $id     [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function updatePage($id, $params)
	{
		$page = Page::find($id);
		$page->content = $params['content']; 

		if($page->save()) {
			return new PageResource($page); 
		}

		return null; 
	}
}