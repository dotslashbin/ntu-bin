<?php
namespace App\Helpers;

/////////////
// Models  //
/////////////
use App\Models\Trash;

///////////////
// Resources //
///////////////
use App\Http\Resources\TrashCollection; 
use App\Http\Resources\TrashResource; 

use Auth; 

use Log;

class TrashesHelper {

	/**
	 * Creates a single trash record
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function createTrash($params) 
	{
		$trash = new Trash(); 

		self::saveTrashProperties($trash, $params);

		if($trash->save()) {
			return new TrashResource(Trash::with('bin')->find($trash->id));
		}

		return null; 
	}

	/** 
	 * Deletes a single trash object
	 */
	public static function deleteTrash($id)
	{
		return Trash::destroy((integer) $id); 	
	}

	/**
	 * Deletes multiple trash records 
	 * @param  Array  $ids [description]
	 * @return [type]      [description]
	 */
	public static function deleteTrashes(Array $ids)
    {
        $hadNoProblems = true; 
        Trash::find($ids)->each(function($trash) {
            if(!$trash->delete()) {
                Log::warning("ERROR: deleting trash -> ".$bin->id);
                $hadProblems = false; 
            } 
        }); 

        return $hadNoProblems; 
    }

	/**
	 * Generates a collection of trashes 
	 * 
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public static function generateTrashes($filters)
	{
		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page'); 

		$results = null; 

		if($filters['bin_id'])
		{
			//////////////////
			// Specific Bin //
			//////////////////
			$trashes  = Trash::where('bin_id', (integer) $filters['bin_id']); 

			$results = $trashes->paginate($itemsPerPage); 

		} else {
			///////////////////////
			// No Specified bin  //
			///////////////////////
			$results = Trash::with('bin')->paginate($itemsPerPage); 
		}

		return new TrashCollection($results); 
	}

	/**
	 * Returns a trash resource object
	 * 
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public static function getTrash($id)
	{
		return new TrashResource(Trash::with('bin')->find($id)); 
	}

	/**
	 * Initiates the properties for the trash object to be saved. 
	 * 
	 * @param  [type] $trash  [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	private static function saveTrashProperties($trash, $params) 
	{

		$trash->selected_type 	= ($params['selected_type'])? $params['selected_type']:'';
		$trash->thrown_type 	= ($params['thrown_type'])? $params['thrown_type']:''; 
		$trash->weight 			= ($params['weight'])? $params['weight']:'';
		$trash->picture 		= ($params['picture'])? $params['picture']:'';
		$trash->bin_id 			= ($params['bin_id'])? $params['bin_id']:''; 
	}

	/**
	 * Updates a trash record 
	 * 
	 * @param  [type] $id     [description]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public static function updateTrash($id, $params)
	{
		$trash = Trash::find((int) $id); 

		self::saveTrashProperties($trash, $params); 
		
		if($trash->save()) {
			return new TrashResource($trash); 
		}

		return null; 
	}
}
