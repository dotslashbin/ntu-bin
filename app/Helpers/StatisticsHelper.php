<?php
namespace App\Helpers; 

//////////////
// Helpers  //
//////////////
use App\Helpers\Common; 

////////////
// Models //
////////////
use App\Models\Bin;
use App\Models\School;
use App\Models\Trash;
use App\Models\User;

////////////////
// Resources  //
////////////////
use App\Http\Resources\StatisticCollection; 

use Auth, DB; 

use Log;

class StatisticsHelper {

	/**
	 * Returns a collection of trash statistics based om the 
	 * given filters. 
	 * 
	 * @param  [type] $filters [description]
	 * @return [type]          [description]
	 */
	public static function generateSatistics($filters) 
	{
		$itemsPerPage = (isset($filters['items_per_page']) && $filters['items_per_page'] > 0)? $filters['items_per_page']:config('app.app_defaults.items_per_page');

			$user = Auth::user(); 

			// DEBUG
			// $user = User::find(5); 	

			switch($user->role_name) {
				case 'super-admin': 
				case 'principal_investigator': 
				case 'co_principal_investigator': 
				case 'research_assistant': 
						$trashes = Trash::with('user', 'bin.school')->paginate($itemsPerPage); 
						return new StatisticCollection($trashes); 
					break;
				case 'student': 
						$trashes = Trash::with('user', 'bin.school')->where('user_id', $user->id)->paginate($itemsPerPage); 
						return new StatisticCollection($trashes); 
					break; 
				case 'teacher':
						$trashes = Trash::with('user', 'bin.school')
											->whereHas('bin', function($query) {
												$user = User::find(Auth::user()->id); 
												$query->where('school_id', $user->school_id); 
											})
											->paginate($itemsPerPage); 
						return new StatisticCollection($trashes); 
					break; 
				default: 
						return response()->json(null,200);
					break; 
			}
		

		// $trashes = Trash::with('user', 'bin.school')->paginate($itemsPerPage); 

		
	}

	/**
	 * Returns a collection following the format of the table to be displayed 
	 * on react-admin. 
	 * 
	 * @param  [type] $trashes [description]
	 * @return [type]          [description]
	 */
	private static function getForamttedCollection($trashes)
	{

	}
}