<?php
////////////////
// Namespaces //
////////////////
namespace App\Helpers;

/////////////
// Models  //
/////////////
use \Spatie\Permission\Models\Role;

////////////////
// Resources  //
////////////////
use App\Http\Resources\RolesCollection; 

use Auth; 

use Log; 

class RolesHelper {

	/**
	 * Returns a collection of roles
	 * @param  boolean $excludeSuper [description]
	 * @return [type]                [description]
	 */
	public static function generateRoles($limitPrivilege = false, $excludeSuper = true) 
	{
		$roles = Role::where('name', '!=', 'super-admin'); 

		if($limitPrivilege) {
			//////////////////////////////////////////////////////
			// Check User's privilege on roles before returning //
			//////////////////////////////////////////////////////
			$roles->whereIn('name', self::getManipulableRoles()); 
		} 

		$result = $roles->get(); 
		return new RolesCollection($result); 
	}

	/**
	 * Returns an array of roles that the user is allowed to modify. 
	 * 
	 * @return [type] [description]
	 */
	private static function getManipulableRoles()
	{
		$roles = []; 
		foreach(Auth::user()->getAllPermissions()->pluck('name')->toArray() as $userPermission) 
		{
			switch ($userPermission) {
				case 'create principal_investigator':
				case 'update principal_investigator':
				case 'delete principal_investigator':
				case 'view principal_investigator':
						if(!in_array('principal_investigator', $roles)) 
						{
							array_push($roles, 'principal_investigator');
						}
					break; 
				case 'create co_principal_investigator':
				case 'update co_principal_investigator':
				case 'delete co_principal_investigator':
				case 'view co_principal_investigator':
						if(!in_array('co_principal_investigator', $roles)) 
						{
							array_push($roles, 'co_principal_investigator');
						}
					break; 
				case 'create research_assistant':
				case 'update research_assistant':
				case 'delete research_assistant':
				case 'view research_assistant':
						if(!in_array('research_assistant', $roles)) 
						{
							array_push($roles, 'research_assistant');
						}
					break; 
				case 'create teacher':
				case 'update teacher':
				case 'delete teacher':
				case 'view teacher':
						if(!in_array('teacher', $roles)) 
						{
							array_push($roles, 'teacher');
						}
					break; 
				case 'create student':
				case 'update student':
				case 'delete student':
				case 'view student':
						if(!in_array('student', $roles)) 
						{
							array_push($roles, 'student');
						}
					break; 
			}
		}

		return $roles;
	}
}