import React from 'react'

import { ChipField, DisabledInput, Edit, ReferenceArrayInput, SelectArrayInput, SimpleForm, TextInput } from 'react-admin'
import Bins from '../../Common/Bins'

export const SchoolFormUpdate = props => (
	<Edit title="Update School details" {...props}>
		<SimpleForm>
			<DisabledInput label="Id" source="id" />
			<TextInput source="name" />
			<TextInput source="address" />

            <ReferenceArrayInput label="Bins" reference="bins" source="bin_ids" filter={{ 'lookup':'1' }} allowEmpty >
            	<SelectArrayInput>
                    <ChipField source="name" />
                </SelectArrayInput>
            </ReferenceArrayInput>
            
		</SimpleForm>
	</Edit>
)