import React from 'react'

import { Create, SimpleForm, TextInput } from 'react-admin'
import Bins from '../../Common/Bins'

export const SchoolFormCreate = props => (
	<Create {...props}>
		<SimpleForm>
			<TextInput source="name" />
			<TextInput source="address" />
			<Bins only_available="true" title="Bins"/>
		</SimpleForm>
	</Create>
)