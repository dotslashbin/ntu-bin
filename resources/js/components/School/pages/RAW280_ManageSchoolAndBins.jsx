import React from 'react'
import { 
	List, 
	Datagrid, 
	TextField
} from 'react-admin';

export const SchoolList = props => {
	return (
		<List {...props}>
			<Datagrid rowClick="edit">
				<TextField source="id" />
				<TextField source="name" />
				<TextField source="address" />
			</Datagrid>
		</List>
	)
}