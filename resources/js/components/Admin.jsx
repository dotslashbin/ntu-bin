/////////////////
// React / RA  //
/////////////////
import React from 'react'
import { Admin, Resource, ListGuesser, fetchUtils, Filter } from 'react-admin'
import { GET_LIST, GET_ONE, GET_MANY, GET_MANY_REFERENCE, CREATE, UPDATE, DELETE } from 'react-admin';

///////////////
// Providers //
///////////////
import jsonServerProvider from 'ra-data-json-server'
import AuthProvider from './User/AuthProvider'
import DataProvider from './Common/DataProvider'

////////////
// Pages  //
////////////
import LoginPage from './User/pages/LoginPage'

////////////////
// Statistics //
////////////////
import { StatList } from './Statistics/pages/RAW2_Homepage' 

///////////
// Users //
///////////
import { UserFormCreate } from './User/pages/RAW211_AddNewAccount'
import { UserFormUpdate } from './User/pages/RAW212_EditAccount'
import { UserList } from './User/pages/RAW21_ManageUserAccount'

/////////////
// Schools //
/////////////
import { SchoolFormCreate } from './School/pages/RAW281_AddNewSchool'
import { SchoolFormUpdate } from './School/pages/RAW282_EditSchoolsAndBins'
import { SchoolList } from './School/pages/RAW280_ManageSchoolAndBins'

//////////////////
// Static Pages //
//////////////////
import { PageUpdate } from './StaticPages/pages/RAW25_EditFAQ'
import { PageShow } from './StaticPages/pages/RAW25_ShowFAQ'

import addUploadFeature from './Common/AddUploadFeature'

const AdminPage = () => ( 
	<Admin dataProvider={ addUploadFeature(DataProvider) } authProvider={ AuthProvider } loginPage={ LoginPage } >
        <Resource name="statistics" list={ StatList } />
        <Resource name="users" list={ UserList } create={ UserFormCreate } edit={ UserFormUpdate }/>
		<Resource name="schools" list={ SchoolList } create={ SchoolFormCreate } edit={ SchoolFormUpdate }/>
		<Resource name="pages" edit={ PageUpdate } show={ PageShow } />
		<Resource name="bins"/>
	</Admin>
)

export default AdminPage;