import React from 'react'
var _ = require('lodash')

const convertToReadable = inputString => {
	return _.startCase(_.toLower(inputString.replace(/_/g, ' ')))
}

export default convertToReadable