/**
 * Convert a `File` object returned by the upload input into a base 64 string.
 * That's not the most optimized way to store images in production, but it's
 * enough to illustrate the idea of data provider decoration.
 */
const convertFileToBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file.rawFile);

    reader.onload = () => setTimeout(resolve(reader.result), 2000);
    reader.onerror = reject;
});

/**
 * Adds an upload feature to the data provider for users
 * @param  {[type]} requestHandler [description]
 * @return {[type]}                [description]
 */
const addUploadFeature = requestHandler => (type, resource, params) => {
    if (resource === 'users' && ( type === 'CREATE' ||  type === 'UPDATE' )) {

        if (params.data.finger_print) {

            return convertFileToBase64(params.data.finger_print)
            .then(base64Image => requestHandler(type, resource, {
                ...params, 
                data: {   
                    ...params.data, 
                    finger_print: base64Image
                }
            })).catch(error => alert(error))
        }
    }
    // for other request types and resources, fall back to the default request handler
    return requestHandler(type, resource, params);
};

export default addUploadFeature;