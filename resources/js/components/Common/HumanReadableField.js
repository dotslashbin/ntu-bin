import React from 'react'
import PropTypes from 'prop-types'
var _ = require('lodash')

const TextField = ({ source, record = {} }) => {

	var readableString = ''; 

	if(typeof record[source] !== 'undefined') {
		readableString = _.startCase(_.toLower(record[source].replace(/_/g, ' ')))
	} else {
		readableString = record[source]
	}
	

	return (
		<span>{ readableString }</span>
	)
}

TextField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
}

export default TextField