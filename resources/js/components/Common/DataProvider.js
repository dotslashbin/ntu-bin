import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
    DELETE_MANY,
    fetchUtils,
    showNotification 
} from 'react-admin'
import { stringify } from 'query-string'

const API_URL = process.env.MIX_APP_URL

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const convertDataProviderRequestToHTTP = (type, resource, params) => {

    switch (type) {
        case GET_LIST: {
            const { page, perPage } = params.pagination
            const { field, order } = params.sort
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
                filter: JSON.stringify(params.filter),
            }

            /** ORIGINAL REFERENCE */
            // return { url: `${API_URL}/${resource}?${stringify(query)}` }
            return { url: `${API_URL}/${resource}?page=${page}&items_per_page=${perPage}&${stringify(query)}` }
        }
        case GET_ONE:{
            return { 
                url: `${API_URL}/${resource}/${params.id}`, 
                user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
            }
        }
        case GET_MANY: {
            const query = {
                filter: JSON.stringify({ id: params.ids }),
            }

            return { url: `${API_URL}/${resource}?${stringify(query)}`}
        }
        case GET_MANY_REFERENCE: {
            const { page, perPage } = params.pagination
            const { field, order } = params.sort
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, (page * perPage) - 1]),
                filter: JSON.stringify({ ...params.filter, [params.target]: params.id }),
            }
            // return { url: `${API_URL}/${resource}?${stringify(query)}` }
            return { url: `${API_URL}/${resource}?` }
        }
        case UPDATE:
            return {
                url: `${API_URL}/${resource}/${params.data.id}`,
                options: { 
                    method: 'PATCH', 
                    body: JSON.stringify(params.data), 
                    user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
                 },
            }
        case CREATE:
            return {
                url: `${API_URL}/${resource}`,
                options: { 
                    method: 'POST', 
                    body: JSON.stringify(params.data), 
                    user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
                 },
            }
        case DELETE:
            return {
                url: `${API_URL}/${resource}/${params.id}`,
                options: { 
                    method: 'DELETE', 
                    user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
                },
            }
        case DELETE_MANY: 
            return {
                url: `${API_URL}/${resource}/delete_many`,
                options: { 
                    method: 'POST', 
                    body: JSON.stringify(params), 
                    user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
                
                }
            }
        default:
        throw new Error(`Unsupported fetch action type ${type}`)
    }
}

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = (response, type, resource, params) => {
    const { headers, body } = response
    const parsedBody = JSON.parse(body)
    switch (type) {
    case GET_LIST:
        return {
            data: parsedBody.data.map(x =>x),
            total: parsedBody.meta.total
        }
    case CREATE:
        return parsedBody
    case UPDATE:
        return parsedBody
    case DELETE:        
        return parsedBody
    case DELETE_MANY:
        return parsedBody
    case GET_MANY:
        return {
            data: parsedBody.data.map(x =>x),
            total: parsedBody.meta.total
        }
    default:
        // return { data: json }
        return parsedBody
    }
}

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */
export default (type, resource, params) => {

    const { fetchJson } = fetchUtils
    const { url, options } = convertDataProviderRequestToHTTP(type, resource, params)
    return fetchJson(url, (options === undefined)? { user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}}:options)
        .then(response => convertHTTPResponseToDataProvider(response, type, resource, params))
        .catch(error => {
            if(error.status > 400) {
                alert(error.message)
                showNotification(error.message, "warning");
                /**
                 * NOTE: this will log the user out
                 */
               // return Promise.reject(error)
            }
        })
}