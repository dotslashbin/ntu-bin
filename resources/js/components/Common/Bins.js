import React, { Component, Fragment } from 'react'
import { SelectArrayInput, fetchUtils } from 'react-admin'
// import axios from 'axios'

const API_URL = process.env.MIX_APP_URL

class Bins extends Component {

	constructor() {
		super()

		this.state = {}
	}

	componentDidMount(props) {

		// this.setState({ bins: [
		// 	{ id:1, name: 'bin0001' }, 
		// 	{ id:2, name: 'bin0002' }, 
		// 	{ id:3, name: 'bin0003' }, 
		// ] })

		// axios.get(`${API_URL}/bins`, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}`} })
		// .then(response => console.log(response)).catch(errors => console.log(errors))
		
		const { fetchJson } = fetchUtils

		var url = `${API_URL}/bins`

		if(this.props.only_available) {
			url = `${API_URL}/bins?available_only=1`
		}

		return fetchJson(
			url, 
			{ 
				user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
			}
		)
        .then(response => {
        	this.setState({ bins: response.json.data })
        })
        .catch(error => {
            console.log("Problem fetching bins")
            console.log(error)
        })
	}


	render() {
		return(
			<Fragment>
				<SelectArrayInput label={ this.props.title } source="bins" choices={ this.state.bins } />
			</Fragment>
		)
	}
}

export default Bins;