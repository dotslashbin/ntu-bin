import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS } from 'react-admin'

import decodeJwt from 'jwt-decode';

export default (type, params) => {
    // called when the user attempts to log in
    if (type === AUTH_LOGIN) {
        const { email, password } = params
        
        const request = new Request('/api/login', {
	        method: 'POST',
	        body: JSON.stringify({ email, password }),
	        headers: new Headers({ 'Content-Type': 'application/json' }),
	    })
	    return fetch(request)
	        .then(response => {
	            if (response.status < 200 || response.status >= 300) {
	                throw new Error(response.statusText);
	            }

	            return response.json();
	        })
	        .then(({ data }) => {
                localStorage.setItem('token', data.token);
                localStorage.setItem('role', data.role);
            });

        return Promise.resolve()
    }
    // called when the user clicks on the logout button
    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('token')
        return Promise.resolve()
    }
    // called when the API returns an error
    if (type === AUTH_ERROR) {
        const { status } = params
        if (status === 401 || status === 403) {
            localStorage.removeItem('token')
            return Promise.reject()
        }
        return Promise.resolve()
    }
    // called when the user navigates to a new location
    if (type === AUTH_CHECK) {
        return localStorage.getItem('token')
            ? Promise.resolve()
            : Promise.reject()
    }

    if (type === AUTH_GET_PERMISSIONS) {
        const token = localStorage.getItem('token');
        if (!token) {
            return Promise.reject();
        }
        const role = localStorage.getItem('role');
        Promise.resolve(role);
    }

    return Promise.reject('Unknown method')
};