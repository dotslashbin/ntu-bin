import React from 'react' 

import Switch from '@material-ui/core/Switch'

const ToggleSound = ({ source, record = {} }) => {
    return(
        <div>
            <span>{ record.id }</span><Switch color="secondary" value="1" />
        </div>
    )
}

export default ToggleSound