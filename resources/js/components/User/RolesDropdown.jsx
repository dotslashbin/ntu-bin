import React, { Component, Fragment } from 'react'
import { SelectInput, fetchUtils } from 'react-admin'
import convertToReadable from '../Common/StringManipulators' 
var _ = require('lodash')

const API_URL = process.env.MIX_APP_URL

class RolesDropdown extends Component {

	constructor() {
		super()

		this.state = {}
	}

	componentDidMount(props) {

		if(this.props.is_edit == "true") {
			this.setState({ selected_role: this.props.record.role_name })
		}

		const { fetchJson } = fetchUtils

		var url = `${API_URL}/roles?limit_privilege=1`
		
		return fetchJson(
			url, 
			{ 
				user: { authenticated: true, token: 'Bearer ' + localStorage.getItem('token')}
			}
		)
        .then(response => {

        	var _ = require('lodash')

        	var roles = []
        	_.forEach(response.json.data, function(role) {
        		roles.push({ id: role.name, name: convertToReadable(role.name) })
        	})

        	this.setState({ roles })
        })
        .catch(error => {
            console.log("Problem fetching roles")
            console.log(error)
        })
	}


	render() {
		return(
			<Fragment>
				<SelectInput label="Role" source="role" choices={ this.state.roles } defaultValue={ this.state.selected_role }/>
			</Fragment>
		)
	}
}

export default RolesDropdown;