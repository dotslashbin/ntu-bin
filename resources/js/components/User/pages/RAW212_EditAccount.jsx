import React from 'react';

import {
  DisabledInput, Edit, ImageInput, ImageField, ReferenceInput, SelectInput, SimpleForm, TextInput
} from 'react-admin';

import RolesDropdown from '../RolesDropdown';

export const UserFormUpdate = props => (
  <Edit title="Update user details" {...props}>
    <SimpleForm>
      <DisabledInput label="Id" source="id" />
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="email" />
      <TextInput source="password" type="password" />
      <ReferenceInput label="School" source="school_id" reference="schools">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <RolesDropdown is_edit="true" />
      <ImageInput source="finger_print" label="Finger print" accept="image/*" >
          <ImageField source="src" title="finger_print" />
      </ImageInput>
    </SimpleForm>
  </Edit>
);
