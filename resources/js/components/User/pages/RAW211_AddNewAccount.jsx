import React from 'react'

import {
  Create, ImageField, ImageInput, FileField, FileInput, ReferenceInput, SelectInput, SimpleForm, TextInput
} from 'react-admin'
import RolesDropdown from '../RolesDropdown'

export const UserFormCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="email" />
      <TextInput source="password" type="password" />
      <ReferenceInput label="School" source="school_id" reference="schools">
        <SelectInput optionText="name" />
      </ReferenceInput>
      <RolesDropdown source="role" />
      <ImageInput source="finger_print" label="Finger print" accept="image/*" >
          <ImageField source="src" title="finger_print" />
      </ImageInput>
    </SimpleForm>
  </Create> 
)