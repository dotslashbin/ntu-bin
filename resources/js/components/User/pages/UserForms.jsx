import React from 'react'

import {
  Create, Edit, SimpleForm, TextInput, DisabledInput, SelectInput, ReferenceInput
} from 'react-admin'

export const UserFormCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="email" />
      <TextInput source="password" />
      <ReferenceInput label="School" source="school_id" reference="schools">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
)

export const UserFormUpdate = props => (
  <Edit title="Update user details" {...props}>
    <SimpleForm>
      <DisabledInput label="Id" source="id" />
      <TextInput source="name" />
      <TextInput source="email" />
      <TextInput source="password" />
      <ReferenceInput label="School" source="school_id" reference="schools">
        <SelectInput optionText="name" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
)
