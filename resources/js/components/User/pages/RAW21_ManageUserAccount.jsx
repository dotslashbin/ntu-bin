import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  EmailField,
  FunctionField,
  BooleanField
} from 'react-admin';

import ToggleSound from '../ToggleSound';
import HumanReadableField from '../../Common/HumanReadableField';

export const UserList = props => (
  <List {...props}>
    <Datagrid rowClick="edit">
      <TextField source="id" />
      <HumanReadableField source="role" />
      <EmailField source="email" />
      <FunctionField label="Name" render={record => `${record.first_name} ${record.last_name}`} />
    </Datagrid>
  </List>
);
