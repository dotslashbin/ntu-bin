import React, { Component } from 'react';
import { connect } from 'react-redux';
import { userLogin } from 'react-admin';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';

class LoginPage extends Component {
  constructor () {
    super();
    this.state = {
      email: '',
      password: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange (event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  onSubmit (event) {
        event.preventDefault();

        const credentials = {
            email: this.state.email,
            password: this.state.password
        };

        this.props.userLogin(credentials);
  }

  render () {
    return (
      <div>
        <div>
                    this is the login page

        </div>
        <div>
          <form onSubmit={this.onSubmit}>
            email:           
            <input type="text" name="email" value={this.state.email} onChange={this.onChange} />
            password:
            <input type="password" name="password" value={this.state.password} onChange={this.onChange} />
            <input type="submit" />
          </form>
        </div>
      </div>
    );
  }
}

export default connect(undefined, { userLogin })(LoginPage);
