import React from 'react' 

import { Datagrid, FunctionField, ImageField, List } from 'react-admin'

export const StatList = props => {
	return (
		<List {...props}>
			<Datagrid>
				<FunctionField label="School" render={ record => `${record.bin.school.name}`} />
				<FunctionField label="Student ID" render={ record => `${record.user.id}`} />
				<FunctionField label="Student Name" render={ record => `${record.user.first_name} ${record.user.last_name}`} />
				<FunctionField label="Trash Selected" render={ record => `${record.selected_type}` }/>
				<FunctionField label="Type of Trash" render={ record => `${record.thrown_type}` }/>
				<FunctionField label="Weight" render={ record => `${record.weight}` }/>
				<FunctionField label="Time Thrown" render={ record => `${record.created_at}` }/>
				<FunctionField label="Bin ID" render={ record => `${record.bin.name}` }/>
				<ImageField source="picture" title="" />
			</Datagrid>
		</List>
	)	
}