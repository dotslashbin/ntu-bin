import React from 'react'
import { SimpleShowLayout, Show, TextField, RichTextField } from 'react-admin'

export const PageShow = (props) => (
    <Show {...props}>
        <SimpleShowLayout>
            <RichTextField source="content" />
        </SimpleShowLayout>
    </Show>
);