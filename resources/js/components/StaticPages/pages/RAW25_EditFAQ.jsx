import React from 'react'
import { Edit, SimpleForm, TextInput } from 'react-admin'
import RichTextInput from 'ra-input-rich-text';

export const PageUpdate= (props) => (
    <Edit title="title" {...props}>
        <SimpleForm>
            <TextInput source="title" />
            <RichTextInput source="content" />
        </SimpleForm>
    </Edit>
);