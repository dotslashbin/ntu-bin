<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NTU BIN</title>
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
        <meta name="csrf-token" content="{{ csrf_token() }}">

    </head>
    <body>
        <p>If you see this, you can customize HTML through blade</p>
        <div id="root"></div>
        <script src="{{mix('js/app.js')}}" ></script>

        @if(config('app.env') == 'local')
            <script src="http://192.168.10.10:35729/livereload.js"></script>
        @endif
    </body>
</html>