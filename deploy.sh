#!/bin/bash
# Change to the project directory
cd /home/forge/ntubin.alphanova.sg

# Turn on maintenance mode
php artisan down

# Pull the latest changes from the git repository
git pull origin master

# Install/update composer dependecies
composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev

# Run database migrations
php artisan migrate --force

# passport 
php artisan passport:install

# Clear caches
php artisan cache:clear

# Clear expired password reset tokens
php artisan auth:clear-resets

# Clear and cache routes
php artisan route:clear
php artisan route:cache

# Clear and cache config
php artisan config:clear
php artisan config:cache

# Install node modules
npm install

# Build assets using Laravel Mix
npm run production

#Key 
php artisan key:generate

# Seeders
php artisan db:seed --class=RolesAndPermissionsSeeder --force
php artisan db:seed --class=SchoolTableSeeder --force
php artisan db:seed --class=SuperuserSeeder --force
php artisan db:seed --class=BinsTableSeeder --force
php artisan db:seed --class=AvailableBinsSeeder --force


# Turn off maintenance mode
php artisan up