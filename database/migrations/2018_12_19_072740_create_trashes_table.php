<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trashes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('selected_type');
            $table->string('thrown_type');
            $table->decimal('weight',8,2);
            $table->string('picture');

            $table->unsignedInteger('user_id'); 
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedInteger('bin_id')->nullable();
            $table->foreign('bin_id')->references('id')->on('bins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trashes');
    }
}
