<?php

use Illuminate\Database\Seeder;

use App\Models\School;

class SchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($counter = 1; $counter <= 30; $counter++) {

            $faker          = Faker\Factory::create();

            $school         = new School; 
            $school->name   = $faker->company(); 
            $school->address   = $faker->address(); 

            if($school->save()) {
                echo "School created ... : ".$school->name."\n"; 
            }
        }
    }
}
