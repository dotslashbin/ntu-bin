<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(SchoolTableSeeder::class);
        $this->call(SuperuserSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(BinsTableSeeder::class);
        $this->call(TrashesTableSeeder::class);
        $this->call(AvailableBinsSeeder::class);
        $this->call(PagesSeeder::class);
    }
}
