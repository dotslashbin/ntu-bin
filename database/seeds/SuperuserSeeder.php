<?php

use Illuminate\Database\Seeder;

use App\Models\User; 

class SuperuserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user 				= new User(); 
        $user->id 			= 1;
        $user->first_name   = 'Joshua'; 
        $user->last_name    = 'Fuentes'; 
        $user->email 		= 'joshua.alphanova@gmail.org'; 
        $user->password 	= bcrypt('secret'); 

        $user->assignRole('super-admin'); 

        if($user->save()) {
        	echo "Superuser created with email: ".$user->email; 
        }
    }
}
