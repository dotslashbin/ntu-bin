<?php

use Illuminate\Database\Seeder;

use App\Models\Bin; 
use App\Models\School; 

class BinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$counter = 1; 

    	while($counter <= 50) {
    		$school = School::all()->random(1)->first(); 

	    	$bin = new Bin; 
	    	$bin->school()->associate($school); 

	    	$bin->save(); 	

	    	echo "New bin: ".$bin->id." for school: ".$school->name."\n"; 

	    	$counter++; 
    	}
        
    }
}
