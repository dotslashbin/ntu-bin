<?php

use Illuminate\Database\Seeder;

////////////
// Models //
////////////
use App\Models\Page;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    ///////////////
    	// FAQs Page //
	    ///////////////
 		$faker 			= Faker\Factory::create();

		$page 			= new Page; 
		$page->title 	= 'FAQ'; 
		$page->content 	= $faker->paragraph(5); 

		$page->save(); 
    }
}
