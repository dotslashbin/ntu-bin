<?php 
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder{
    public function run()
    {
        /////////////////////////////////////////
        // Reset cached roles and permissions  //
        /////////////////////////////////////////
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        ////////////////////////
        // Create permission  //
        ////////////////////////
        Permission::create(['name' => 'create principal_investigator']);
        Permission::create(['name' => 'update principal_investigator']);
        Permission::create(['name' => 'delete principal_investigator']);
        Permission::create(['name' => 'view principal_investigator']);

        Permission::create(['name' => 'create co_principal_investigator']);
        Permission::create(['name' => 'update co_principal_investigator']);
        Permission::create(['name' => 'delete co_principal_investigator']);
        Permission::create(['name' => 'view co_principal_investigator']);

        Permission::create(['name' => 'create research_assistant']);
        Permission::create(['name' => 'update research_assistant']);
        Permission::create(['name' => 'delete research_assistant']);
        Permission::create(['name' => 'view research_assistant']);

        Permission::create(['name' => 'create teacher']);
        Permission::create(['name' => 'update teacher']);
        Permission::create(['name' => 'delete teacher']);
        Permission::create(['name' => 'view teacher']);

        Permission::create(['name' => 'create student']);
        Permission::create(['name' => 'update student']);
        Permission::create(['name' => 'delete student']);
        Permission::create(['name' => 'view student']);

        Permission::create(['name' => 'create school']);
        Permission::create(['name' => 'update school']);
        Permission::create(['name' => 'delete school']);
        Permission::create(['name' => 'view school']);

        Permission::create(['name' => 'create bin']);
        Permission::create(['name' => 'update bin']);
        Permission::create(['name' => 'delete bin']);
        Permission::create(['name' => 'view bin']);

        Permission::create(['name' => 'create survey']);
        Permission::create(['name' => 'update survey']);
        Permission::create(['name' => 'delete survey']);
        Permission::create(['name' => 'view survey']);

        Permission::create(['name' => 'create faq']);
        Permission::create(['name' => 'update faq']);
        Permission::create(['name' => 'delete faq']);
        Permission::create(['name' => 'view faq']);

        Permission::create(['name' => 'create notifications']);
        Permission::create(['name' => 'view notifications']);

        Permission::create(['name' => 'survey_participate']);
        Permission::create(['name' => 'push_notifications']);
        Permission::create(['name' => 'statistics_view']);
        Permission::create(['name' => 'dashboard_leftside_menu']);

        ////////////////////////////
        // Principal Investigator //
        ////////////////////////////
        $role = Role::create(['name' => 'principal_investigator'])
            ->givePermissionTo([
                 'update co_principal_investigator',
                 'delete co_principal_investigator',
                 'view co_principal_investigator',
                 'create research_assistant',
                 'update research_assistant',
                 'delete research_assistant',
                 'view research_assistant',
                 'create teacher',
                 'update teacher',
                 'delete teacher',
                 'view teacher',
                 'create student',
                 'update student',
                 'delete student',
                 'view student',
                 'create survey',
                 'update survey',
                 'delete survey',
                 'view survey',
                 'create faq',
                 'update faq',
                 'delete faq',
                 'view faq',
                 'create notifications', 
                 'view notifications', 
                 'survey_participate',
                 'push_notifications',
                 'statistics_view',
                 'dashboard_leftside_menu'
            ]);

        ////////////////////////////////
        // Co-Principal Investigator  //
        ////////////////////////////////
        $role = Role::create(['name' => 'co_principal_investigator'])
            ->givePermissionTo([
                 'create research_assistant',
                 'update research_assistant',
                 'delete research_assistant',
                 'view research_assistant',
                 'create teacher',
                 'update teacher',
                 'delete teacher',
                 'view teacher',
                 'create student',
                 'update student',
                 'delete student',
                 'view student',
                 'create survey',
                 'update survey',
                 'delete survey',
                 'view survey',
                 'create faq',
                 'update faq',
                 'delete faq',
                 'view faq',
                 'create notifications', 
                 'view notifications', 
                 'survey_participate',
                 'push_notifications',
                 'statistics_view',
                 'dashboard_leftside_menu'
            ]);

        /////////////////////////
        // Research Assistant  //
        /////////////////////////
        $role = Role::create(['name' => 'research_assistant'])
            ->givePermissionTo([
                 'create teacher',
                 'update teacher',
                 'delete teacher',
                 'view teacher',
                 'create student',
                 'update student',
                 'delete student',
                 'view student',
                 'create survey',
                 'update survey',
                 'delete survey',
                 'view survey',
                 'create faq',
                 'update faq',
                 'delete faq',
                 'view faq',
                 'create notifications', 
                 'view notifications', 
                 'survey_participate',
                 'push_notifications',
                 'statistics_view',
                 'dashboard_leftside_menu'
            ]);

        //////////////
        // Teacher  //
        //////////////
        $role = Role::create(['name' => 'teacher'])
            ->givePermissionTo([
                 'create student',
                 'update student',
                 'delete student',
                 'view student',
                 'view survey',
                 'view faq',
                 'view notifications', 
                 'survey_participate',
            ]);

        ///////////////
        // student  //
        ///////////////
        $role = Role::create(['name' => 'student'])
            ->givePermissionTo([
                 'view survey',
                 'view faq',
                 'view notifications', 
                 'survey_participate',
            ]);

        //////////////////
        // Super Admin  //
        //////////////////
        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
    }
}