<?php

use Illuminate\Database\Seeder;

use App\Models\Bin;

class AvailableBinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counter = 1; 

    	while($counter <= 50) {

	    	$bin = new Bin; 

	    	$bin->save(); 	

	    	echo "New bin: ".$bin->id; 

	    	$counter++; 
    	}
    }
}
