<?php

use Illuminate\Database\Seeder;

use App\Models\School; 
use App\Models\User; 

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $users = factory(App\Models\User::class, 1000)->create(); 
        $roles      = ['principal_investigator', 'co_principal_investigator', 'research_assistant', 'teacher', 'student']; 
        $schoolIds  = School::get()->pluck('id')->toArray(); 

        for($counter = 1; $counter <= 50; $counter++) {

            $faker              = Faker\Factory::create();

        	$user 				= new User();

            $user->first_name   = $faker->firstName();
	        $user->last_name    = $faker->lastName();
	        $user->email 		= $faker->unique()->safeEmail;
	        $user->password 	= bcrypt('secret'); 
            $user->school_id    = $schoolIds[rand(1, (count($schoolIds) - 1))]; 

	        $user->assignRole($roles[rand(1,4)]); 

	        if($user->save()) {
	        	echo "User created with email: ".$user->email."\n"; 
	        }
        }
    }
}
