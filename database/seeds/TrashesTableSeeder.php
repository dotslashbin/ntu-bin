<?php

use Illuminate\Database\Seeder;

use App\Models\Bin; 
use App\Models\School; 
use App\Models\Trash; 
use App\Models\User; 


class TrashesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $students = User::whereHas('roles', function($query) {
            $query->where('name', 'student'); 
        })->get(); 

        foreach($students as $student) {
            $totalTrash     = rand(1, 10); 
            $counter        = 1; 
            while($counter <= $totalTrash) {
                $counter++; 

                $bin        =  Bin::whereNotNull('school_id')->get()->random(1)->first();

                $student->school_id = $bin->school_id; 
                $student->save(); 

                $trash                      = new Trash; 
                $trash->bin()->associate($bin); 

                $randomKey = array_rand(config('app.app_defaults.trash_types'), 1); 

                $trash->selected_type       = config('app.app_defaults.trash_types')[$randomKey];
                $trash->thrown_type         = config('app.app_defaults.trash_types')[$randomKey];
                $trash->weight              = rand(1, 1000);
                $trash->picture             = 'storage/trash.jpg';
                $trash->user_id             = $student->id; 

                if($trash->save()) {
                    echo "New $trash: ". $trash->id." for bin: ".$bin->name." by user: ".$student->email."\n"; 
                }   
            }   
        }


     //    $counter = 1; 
    	// while($counter <= 50) {
    	// 	$bin = Bin::all()->random(1)->first(); 

	    // 	$trash = new Trash; 
	    // 	$trash->bin()->associate($bin); 
	    // 	$trash->selected_type = 'paper';
	    // 	$trash->thrown_type = 'paper';
	    // 	$trash->weight = 10;
	    // 	$trash->picture = 'somewhere in the net';

	    // 	$trash->save(); 	

	    // 	echo "New $trash: ". $trash->id." for bin: ".$bin->name."\n"; 

	    // 	$counter++; 
    	// }
    }
}
