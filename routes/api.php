<?php

use Illuminate\Http\Request;

///////////////////
// test includes //
///////////////////
use App\Models\User; 
use App\Models\School; 
use App\Models\Bin; 
use App\Http\Resources\UserCollection; 
use App\Http\Resources\SchoolCollection; 
use App\Http\Resources\BinCollection; 
use App\Http\Resources\MessageResource; 

use App\Helpers\Common; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

////////////////////
// Authentication //
////////////////////
Route::apiResource('login', 'Auth\LoginController'); 

///////////
// Bins  //
///////////
Route::name('bins')->middleware('auth:api')->group(function() {
	Route::apiResource('bins', 'BinsController')->only(['store', 'update', 'delete'])->middleware(['auth:api', 'role:super-admin|principal_investigator|co_principal_investigator|research_assistant']); 	
	Route::apiResource('bins', 'BinsController')->only(['index']); 
});
Route::post('bins/delete_many', 'BinsController@destroyMany')
		->middleware(['auth:api', 'role:super-admin']); 
Route::post('bins/get_token', 'BinsController@getToken'); 

////////////
// Pages  //
////////////
Route::apiResource('pages', 'PagesController')->only(['index', 'show']); 
Route::apiResource('pages', 'PagesController')->only(['delete', 'update', 'store'])
		->middleware(['auth:api', 'role:super-admin|principal_investigator|co_principal_investigator|research_assistant']); 

////////////
// Roles  //
////////////
Route::apiResource('roles', 'RolesController')->middleware('auth:api'); 

///////////
// Users //
///////////
// Route::name('users')->middleware('auth:api')->group(function() {
// 	Route::apiResource('users', 'UsersController')->only(['index', 'show', 'store', 'update']); 
// 	Route::apiResource('users', 'UsersController')->only(['delete'])->middleware('role:super-admin|principal_investigator|co_principal_investigator|research_assistant'); 
// });
// Route::post('users/delete_many', 'UsersController@destroyMany')->middleware(['auth:api', 'role:super-admin|principal_investigator|co_principal_investigator|research_assistant']); 
Route::apiResource('users', 'UsersController')->middleware('auth:api'); 
Route::post('users/delete_many', 'UsersController@destroyMany')->middleware('auth:api');

/////////////
// Schools //
/////////////
// Route::apiResource('schools', 'SchoolsController')->middleware('auth:api'); 
Route::name('schools')->middleware('auth:api')->group(function() {
	Route::apiResource('schools', 'SchoolsController')
				->only(['store', 'delete', 'show', 'update'])
				->middleware('role:super-admin|principal_investigator|co_principal_investigator|research_assistant'); 
	Route::apiResource('schools', 'SchoolsController')
				->only(['index']); 
				
}); 

Route::post('schools/delete_many', 'SchoolsController@destroyMany')
		->middleware(['auth:api', 'role:super-admin|principal_investigator|co_principal_investigator|research_assistant']); 

/////////////////
// Statistics  //
/////////////////
Route::apiResource('statistics', 'StatisticsController')->middleware('auth:api');

//////////////
// Trashes  //
//////////////
Route::name('trash')->middleware('auth:api')->group(function() {
	Route::apiResource('trashes', 'TrashesController')->only(['delete', 'update'])->middleware('role:super-admin|principal_investigator|co_principal_investigator|research_assistant'); 
	Route::apiResource('trashes', 'TrashesController')->only(['index']); 
}); 
Route::post('trashes/delete_many', 'TrashesController@destroyMany')->middleware('auth:api'); 



////////////
// Tests  //
////////////
Route::get('test', function() {
	$x = Bin::find(1);


	return $x->createToken('alphanova')->accessToken; 
});
